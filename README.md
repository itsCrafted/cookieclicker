CookieClicker
=============

This is a copy of Orteil's cookie clicker, and then some. The original game can be found at http://orteil.dashnet.org/cookieclicker/,
where the source is publicly available.

The original game will be kept in the 'master' branch, if any changes are made, it will be pushed to the 'modified' branch.

Feel free to fork this etcetera. Have fun (and remember that this may not stay updated)!
